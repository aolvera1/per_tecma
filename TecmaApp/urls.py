"""TecmaApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.views.generic.base import RedirectView
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from TecmaMain import views

# favicon view
favicon_view = RedirectView.as_view(
    url='/static/archivosTecma/TecmaGeneral/favicon.ico',
    permanent=True)

urlpatterns = [
    url(r'^$', views.user_login),
    url(r'^/', views.user_login),
    url(r'^admin/', admin.site.urls),
    url(r'^login/', views.user_login),
    url(r'^logoff/', views.user_logout),
    url(r'^favicon\.ico$', favicon_view),
    url(r'^sitio_reclutador/reclutadormain.html', views.reclutadormain),
    url(r'^sitio_reclutador/metasreclutador.html', views.metasview),
    url(r'^sitio_reclutador/detalleproyecto.html', views.DetalleProyecto),
    url(r'^sitio_reclutador/registroreclutador.html', views.reclutarview),
    url(r'^sitio_reclutador/minuevacasa.html', views.minuevacasa),
    url(r'^sitio_reclutador/mifamiliatecma.html', views.mifamiliatecma),
    url(r'^sitio_reclutador/mipago.html', views.mipago),
    url(r'^sitio_reclutador/misprestaciones.html', views.misprestaciones),
    url(r'^sitio_reclutador/milugardetrabajo.html', views.milugardetrabajo),
    url(r'^sitio_rh/rhmain.html', views.rhmain),
    url(r'^sitio_rh/rhprocess.html', views.procesorh),
    url(r'^sitio_coordinador/coordinarmain.html', views.coordinarmain),
    url(r'^sitio_coordinador/vistareportes.html', views.reportesExport),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
