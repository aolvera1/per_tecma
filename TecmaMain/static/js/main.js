/*
 Funcion para el selector de fechas de la modal
 registro de metas de reclutador
 /static/js/main.js
*/

$(document).ready(function () {
    $('#metasdate').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        defaultDate: new Date()            	
   	}).datepicker("setDate", "0");
});

/*
 Selector de fecha modal Reporte de metas
*/
$(document).ready(function () {
    $('#repodate').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        defaultDate: new Date()            	
   	}).datepicker("setDate", "0");
});


/*
 Selector de fecha modal Lista de Reclutados
*/
$(document).ready(function () {
    $('#repolista').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        defaultDate: new Date()            	
   	}).datepicker("setDate", "0");
});


/*
 Selector de fechas de uso general
*/

$(document).ready(function () {
    $('#rhlistas').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        defaultDate: new Date()
    }).datepicker("setDate", "0");
});


/*
 Selector de fechas de uso general
*/

$(document).ready(function () {
    $('#date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        defaultDate: new Date()
    }).datepicker("setDate", "0");
});


/*
 Selector de fechas para #fechanacimiento
*/
$(document).ready(function () {
	$('#fechanacimiento').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        defaultDate: new Date()            	
	}).datepicker("setDate", "0");
});



/*
 Deshabilitar el boton de input cuando se realiza un submit
 Previene errores por el dobleclick
*/
$('.btndisable').on('submit', function() {

    console.log('Enviado');
    
    // Al detectar la primera accion, el boton se deshabilita
    var self = $(this),
	button = self.find('input[type="submit"], button');
    button.attr('disabled', 'disabled').val('Enviando');
        
});

/*
 Chart.js config/uso
*/

var ctx = $('#myChart');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'horizontalBar',

    // The data for our dataset
    data: {
        // Esto seria el conteo
        labels: ["0", "1", "2", "3", "4", "5", "6","7","8"],
        datasets: [{
            label: "Conteo",
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [1,1,2,3,4,2,3,4,2]
        }]
    },

    // Configuration options go here
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            
            }],
            xAxes: [{
                ticks: {
                    beginAtZero:true
                }
            
            }]
        }
    
    }
});

/*
 Export to CSV functions. The first one produces the CSV data. The second one grabs
 the data ans starts the dialog to save the resulting file.
*/

function exportTableToCSV(filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        
        csv.push(row.join(","));        
    }

    // Download CSV file
    downloadCSV(csv.join("\n"), filename);
}

function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV file
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Hide download link
    downloadLink.style.display = "none";

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
}

