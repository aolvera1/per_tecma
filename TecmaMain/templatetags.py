from TecmaMain.models import Puesto
from django import template


# Templates para construir las opciones de los drop boxes

register = template.Library()


@register.inclusion_tag('.html')
def puestosproyecto():
    """ Funcion para regresar los valores de los puestos"""
    puestos_lista = Puesto.objects.all()
    return {'puestosLista': puestos_lista}
